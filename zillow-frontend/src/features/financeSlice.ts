import { createSlice, PayloadAction } from '@reduxjs/toolkit'
 
export interface FinanceState{
    interest: string,
    principal: string,
    term: string,
    closingCost: string,
    rental: string,
    entryCost: string,
    occupancy: string,
    exitProceeds:string,
    operatingCost:string,
    taxCost: string,
    purchasePrice: string,
    count:number,

}
 
const initialState:FinanceState = {
    interest: "0.05",
    principal: "100000",
    term: "240",
    closingCost: "2000",
    rental: "1500",
    entryCost: "5000",
    occupancy: "0.9225",
    exitProceeds: "115000",
    operatingCost:"40",
    taxCost:"1200",
    purchasePrice:"120000",
    count:1

};

export const financeSlice = createSlice({
    name: 'finance',
    initialState,
    reducers: {
        addFinance: (state, action:PayloadAction<number>)=>{
            console.log(action.payload)
               return void action.payload
        },
        updateFinance: (state, action:PayloadAction<number>)=>{
            console.log(action.payload)
               return void action.payload
        },
        //ASSUMPTIONS REDUCERS
        addInterest:(state, {payload})=>{state.interest=payload;},
        addPrincipal:(state, {payload})=>{state.principal=payload;},
        addTerm:(state, {payload})=>{state.term=payload;},
        addClosingCost:(state, {payload})=>{state.closingCost=payload;},
        addRental:(state, {payload})=>{state.rental=payload;},
        addEntryCost:(state, {payload})=>{state.entryCost=payload;},
        addOccupancy:(state, {payload})=>{state.occupancy=payload;},
        addOperatingCost:(state, {payload})=>{state.operatingCost=payload;},
        addExitProceeds:(state, {payload})=>{state.exitProceeds=payload;},
        addTaxCost:(state, {payload})=>{state.taxCost=payload;},
        addPurchasePrice:(state, {payload})=>{state.purchasePrice=payload;},
    },

});

export const {addFinance, updateFinance, addInterest,addPrincipal,addTerm,addClosingCost,addRental, addEntryCost, addOccupancy, addOperatingCost, addExitProceeds,addTaxCost,addPurchasePrice} = financeSlice.actions
export default financeSlice.reducer;

