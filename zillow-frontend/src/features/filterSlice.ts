import { createSlice, PayloadAction } from '@reduxjs/toolkit'
export interface FilterState{
    id: number,
    priceMin: number,
    priceMax: number,
    roomsMin: number,
    bedsMin: number,
    yearOldest: number,
    daysOldest: number
}
 
const initialState:FilterState = {
    id:1,
    priceMin: 0,
    priceMax: 0,
    roomsMin: 0,
    bedsMin: 0,
    yearOldest: 1900,
    daysOldest: 0
};

export const filterSlice = createSlice({
    name: 'filters',
    initialState,
    reducers: {
        addFilter: (state, action:PayloadAction<FilterState>)=>{
            console.log(state.id, state.priceMin, state.priceMax, state.roomsMin,state.bedsMin,state.yearOldest,state.daysOldest);
            //console.log("the next thing is then to have the filter state used in filtering the list of properties");
        },
        clearFilter:(State)=>{
            return initialState;
        },
        addPriceMin:(state, {payload})=>{  
            state.id=Date.now();
            state.priceMin=payload;
            console.log(state.priceMin);
        }, 
        addPriceMax:(state, {payload})=>{
            state.id=Date.now();
            state.priceMax=payload;
            console.log(state.priceMax);
        }, 
        addRoomsMin:(state, {payload})=>{
            state.id=Date.now();
            state.roomsMin=payload;
            console.log(state.roomsMin);
        }, 
        addBedsMin:(state, {payload})=>{ 
            state.id=Date.now();
            state.bedsMin=payload;
            console.log(state.bedsMin);
        }, 
        addYearOldest:(state, {payload})=>{
            state.id=Date.now();
            state.yearOldest=payload;
            console.log(state.yearOldest);
        }, 
        addDaysOldest:(state, {payload})=>{
            state.id=Date.now();
            state.daysOldest=payload;
            console.log(state.daysOldest);
        }, 
        
    }
});

export const {addFilter, clearFilter, addPriceMin,addPriceMax,addRoomsMin,addBedsMin,addYearOldest,addDaysOldest} = filterSlice.actions
export default filterSlice.reducer;

