import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';

import { Place } from '../models/places.model';

//https://www.youtube.com/watch?v=vsxJSc-Q7CA

//We need to keep the state out of this API call... it should be passed as a parameter

export const placeApi=createApi({
    reducerPath: 'placeApi',
    baseQuery: fetchBaseQuery({baseUrl: "http://98.175.4.210:8989/api/v1/test"}),//this is our base url for our api
    endpoints: (builder)=>({
        places: builder.query<Place,{limit: number,latitude:number,longitude:number, radius: number}>({
            query:(args)=>{
                const {limit, latitude, longitude, radius} = args;
                return{
                    url: 'circleSearch',
                    params: {limit, latitude, longitude, radius},
                    method: 'GET'
                };
            }
        }),
        filterPlaces: 
        builder.query<Place,{limit: number,latitude:number,longitude:number, radius: number, query: string}>({
            query:(args)=>{
                const {limit, latitude, longitude, radius, query} = args;
                return{
                    url: 'zillow/search',
                    params: {limit, latitude, longitude, radius, query},
                    method: 'GET'
                };
            }
        }),
    })

})

//this export naming is keyed to the name of the built query
export const {usePlacesQuery, useFilterPlacesQuery} = placeApi;