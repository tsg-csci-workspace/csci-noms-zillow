import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import * as L from 'leaflet';
import { latLng, leafletElement } from '../models/leafletElement';

const initialState: leafletElement = {
    radius: 1,
    latLng: {lat: 34.11589652, lng: -119.12 } as latLng,
    layerType: ""
}

export const mapSlice = createSlice({
    name: 'map',
    initialState,
    reducers: {
        setLayer: (state, action:PayloadAction<leafletElement>)=>{
               return {...state, radius: action.payload.radius, 
                layerType: action.payload.layerType,
                latLng: action.payload.latLng};
        },
        resetLayer:(state) =>{
            return initialState;
        }
    },

});

export const {setLayer, resetLayer} = mapSlice.actions
export default mapSlice.reducer;
