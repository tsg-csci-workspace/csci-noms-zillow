import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Bundle } from '../models/places.model';
export interface PlaceState{

    ListingId: String,
    //ShortDescription: String,
    Latitude: number,
    Longitude: number,
    RoomsTotal: number,
    BedroomsTotal: number,
    BathroomsFull: number,
    BathroomsTotalDecimal:number,
    YearBuilt: number,
    DaysOnMarket:number,
    ListPrice:number,
    LivingArea:number,
    //Visible:boolean
}
 
const initialState:Bundle = {
    ListingId: "1",
    //ShortDescription: "999 Hucckls",
    Latitude: 45,
    Longitude: -95,
    RoomsTotal: 4,
    BedroomsTotal: 2,
    BathroomsFull: 1,
    BathroomsTotalDecimal:2,
    YearBuilt: 1985,
    DaysOnMarket:33,
    ListPrice:333333,
    LivingArea:2400,
    //Visible: true
    
};

export const placeSlice = createSlice({
    name: 'place',
    initialState,
    reducers: {
        setPlace: (state, action:PayloadAction<Bundle>)=>{
               return action.payload;

        },
        removePlace: (state, action: PayloadAction<number>)=>{
            state.ListingId.slice(action.payload,1);
        },
/*         togglePlace(state, action: PayloadAction<PlaceState>) {
            let place = state.find(place => place.ListingId === action.payload.ListingId);
            if (place) {
                place.Visible = !place.Visible;
            }
        },  */
    },

});

export const {setPlace, removePlace} = placeSlice.actions
export default placeSlice.reducer;

