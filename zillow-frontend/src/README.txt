

Rev 2/15/2022 - Chuck
 - API: the api call to a fake api works, and displays data for the selected attributes
   - when we get our api endpoints we'll need to validate
   - the shortDescription attribute has a longer name in the zillow api that includes a decimal, 
     not sure how to handle that

 
 - FILTER: The range slide will not work correctly, this is a browser issue, so this will have to be removed

 - MAP: The leaflet information is current from the development branch, and includes the ability to persist shapes

 27B Adds:
  - financeSlice
  -/components/Assumptions
  -/components/Analysis
  -/app/rootReducer
