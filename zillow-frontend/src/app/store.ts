import { Action, configureStore, ThunkAction} from "@reduxjs/toolkit";
import placeReducer from '../features/placeSlice';
import filtersReducer from '../features/filterSlice';
import financeReducer from '../features/financeSlice';
import mapReducer from '../features/mapSlice';
import { placeApi } from "../features/placeApi";

import {useDispatch} from 'react-redux';
import { setupListeners } from "@reduxjs/toolkit/dist/query";


 
export const store = configureStore({
    reducer: {
        place: placeReducer,
        [placeApi.reducerPath]:placeApi.reducer,
        filters: filtersReducer,
        map: mapReducer,
        finance: financeReducer,
    },

    //middleware: getDefaultMiddleware => getDefaultMiddleware({serializableCheck: false}).concat(thunk),
    middleware: (getDefaultMiddleware)=>getDefaultMiddleware().concat(placeApi.middleware)
});

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>()
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

setupListeners(store.dispatch);

export default store;
