import { combineReducers } from '@reduxjs/toolkit'

import filters from '../features/filterSlice'
import finance from '../features/financeSlice'
import places from '../features/placeSlice'

const rootReducer = combineReducers({
  filters,
  finance,
  places
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer