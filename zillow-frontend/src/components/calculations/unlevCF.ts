import React from 'react'
import Calculations from './Calculations';

//MONTHLY UNLEVERAGED CASH FLOW *********************{UDPATED}
export function getMoULCF(pp:number, termMo:number, ep:number, noi:number, cc:number):number[]{
    //where: pp=purchase price, termMo=loan term in months, ep=net exit proceeds, noi=net operating income monthly
  const arrUnlevCashFlow:number[]=[];
  for (var i = 1; i <= termMo; i++) {
    if(i===termMo){
      arrUnlevCashFlow.push(ep+noi);
    }else if(i>0){
      arrUnlevCashFlow.push(noi);
    }
  }
  arrUnlevCashFlow.unshift(-pp-cc)
  return (
    arrUnlevCashFlow
  )
}


//YEARLY UNLEVERAGED CASH FLOW***************{UPDATED}
export function getYrULCF(MULCF:number[], termMo:number,pp:number, cc:number):number[]{
var arrUnlevYrCashFlow:number[]=[];
var YrValue:number=0;
  for (var i = 1; i <= termMo; i++) {
    if((i===termMo)||(i%12===0)){
        //post a value for annual cash flow
        YrValue+=MULCF[i];
        arrUnlevYrCashFlow.push(YrValue);
        //set summation to zero
        YrValue=0;
    }else{
      //continue to sum monthly cash flows
      YrValue+=MULCF[i];
    }
  }
  arrUnlevYrCashFlow.unshift(-pp-cc);
  return (
    arrUnlevYrCashFlow
  )
}





