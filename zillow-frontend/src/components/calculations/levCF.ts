import React from "react";
import Calculations from './Calculations';


 //MONTHLY LEVERAGED CASH FLOW: *******************{UPDATED}
export function getMoLCF(princ:number, termMo:number, ccost:number, moulcf:number[], modp:number,pp:number):number[]{
    const arrMoLevCashFlow:number[]=[];
    for (var i = 1; i <= termMo; i++) {
        if(i===termMo){
            arrMoLevCashFlow.push(moulcf[i]-modp)
        } else{
            arrMoLevCashFlow.push(moulcf[i]-modp);
        }
    }
    arrMoLevCashFlow.unshift(-pp-ccost+princ)
    return(
        arrMoLevCashFlow
    )
}

 
  //  cashflows inclusive of the debt servicing



  // YEARLY LEVERAGED CASH FLOW: ***********************
  export function getYrLCF(molcf:number[], termMo:number, pp:number, cc:number, princ:number):number[]{
        var arrYrLevCashFlow:number[]=[];
        var YrValue:number=0;
        for (var i = 1; i <= termMo; i++) {
            if((i%12===0)){
                YrValue+=molcf[i];
                arrYrLevCashFlow.push(YrValue);
                YrValue=0;
            }else if((i===termMo)){
                //post a value for annual cash flow
                YrValue+=molcf[i];
                arrYrLevCashFlow.push(YrValue);
                //set summation to zero
                YrValue=0;
            }else{
            //continue to sum monthly cash flows
            YrValue+=molcf[i];
            }
        }
        arrYrLevCashFlow.unshift(-pp-cc+princ)
    return(
        arrYrLevCashFlow
        )
  }

