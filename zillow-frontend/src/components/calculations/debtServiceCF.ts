import React from 'react'
import Calculations from './Calculations';


// MONTHLY DEBT SERVICE *********************************** {UPDATED}
var moInt:number=0;

export function getBalance(princ:number, term:number, intYr:number, clcost:number):number[] {
  var payment:number=0;
  var intMo:number=0;
  intMo = intYr / 12;
  var balance:number=princ;
  var interest:number=princ*intMo;
  var reduction:number=0;
  const theBalance:number[]=[];

  if ((princ == null ) || (term == null ) || (intYr == null))  { 
    //we have incomplete info for calculation;
  }  else  {
    if(intYr===0){
      payment=balance/term;
    } else{
      payment = balance * intMo / (1 - (Math.pow(1/(1 + intMo), term)));
    }
    
      theBalance.push(balance);
      for (var i = 1; i <= term; i++) {
          interest=theBalance[i-1]*intMo;
          reduction=payment-interest;
          balance=theBalance[i-1]-reduction;
          theBalance.push(balance);
      }
      
  }
    return (theBalance)//monthly balance
 }

export default function getMoDebtService(princ:number, term:number, intYr:number, clcost:number):number {
  var payment:number=0;
  var intMo:number=0;
  intMo = intYr / 12;
  var balance:number=princ;

  if ((princ == null ) || (term == null ) || (intYr == null))  { 
    //we have incomplete info for calculation;
    }else if(princ===0||intYr===0){
      payment=0;
    }  else {
      payment = balance * intMo / (1 - (Math.pow(1/(1 + intMo), term)));
    }
    return (payment)//monthly debt payment with interest
 }

// YEARLY DEBT SERVICE CASH FLOW: ***********************
 export function getYrDebtService(princ:number, termMo:number, intYr:number, clcost:number):number[]{
         
        var mds:number=getMoDebtService(princ,termMo,intYr,clcost);
        var ads:number[]=[];
        var paySum:number=0;

          for (var i = 1; i <= termMo; i++) {
            if(princ===0||intYr===0){
              ads.push(paySum)
            }else if((i%12==0)||(i==termMo)){
                //post a value for annual cash flow
                paySum+=mds;
                ads.push(paySum)
                paySum=0;
            }else{
              //continue to sum monthly cash flows
              paySum+=mds;
            }
          }
          ads.unshift(0);
           return (ads)//monthly debt payment with interest
 }
