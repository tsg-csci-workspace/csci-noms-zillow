import React from 'react';

export function GetNPV(cf:number[],intMo:number):number{
    var theNPV:number=0;
    for (var i = 0; i < cf.length; i++) {
      theNPV=theNPV+cf[i]/(Math.pow((1+intMo),i));
    }
    return theNPV;
  }