import React from 'react';

export function SumCash(cf:number[]):number{
    var cfTot:number=0;
    for (var i = 0; i < cf.length; i++) {
      cfTot+=cf[i];
    }
    return cfTot;
  }