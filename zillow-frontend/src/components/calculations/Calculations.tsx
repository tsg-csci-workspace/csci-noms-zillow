import React from 'react'
import { useState } from 'react';
import {useDispatch, useSelector} from 'react-redux'
import {store, RootState} from '../../app/store';

import Assumptions from '../Assumptions';
import Analysis from '../Analysis';
import rootReducer from '../../app/xxxrootReducer';
import { irr } from 'node-irr';
import getDebtService, { getBalance, getYrDebtService } from './debtServiceCF';
import getAnnualDebtService from './debtServiceCF';
import { getMoULCF, getYrULCF } from './unlevCF';
import { getMoLCF, getYrLCF } from './levCF';
import {SumCash} from './sumCF';
import {GetNPV} from './netPresentValue';
import { Table } from 'semantic-ui-react';
import getMoDebtService from './debtServiceCF';
import CurrencyFormat from 'react-currency-format'


export function formatToCurrency(amount){
  return (amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'); 
}

const Calculations = (state) => {
  
  const financeState = useSelector((state:RootState) =>state.finance);
  
  //CONVERT STRINGS TO NUMBERS 
  const numRental=Number(financeState.rental);//revenue from renting unit
  const numVacancy=1-Number(financeState.occupancy);//fraction of time the units are vacant, thus no rental income
  const numInterest=Number(financeState.interest);//annual interest rate on loan, expressed as decimal
  const numPrincipal=Number(financeState.principal);//amount of money loaned
  const numTerm=Number(financeState.term);
  const numClosingCost = Number(financeState.closingCost);
  const numEntryCost = Number(financeState.entryCost);
  const numOperatingCost=Number(financeState.operatingCost);
  const numExitProceeds=Number(financeState.exitProceeds);
  const numTaxCost=Number(financeState.taxCost);//includes tax and insurance - annual
  const numPurchasePrice=Number(financeState.purchasePrice);
  var MOC:number = numOperatingCost+numTaxCost/12 //***** MONTHLY OP COST */
  var DEBT:number=numPrincipal;
  var EQUITY:number=numPurchasePrice+numClosingCost+numEntryCost-DEBT;
  
 
  console.log("Monthly Op Cost: " + JSON.stringify(MOC));


//GROSS INCOME **********************************{OK}
  const grossIncome:number=numRental-((numVacancy)*numRental);

//OPERATING INCOME NET B4 DEBT SVC ******************************{OK}
  const MNOIBDS:number = grossIncome-numOperatingCost-numTaxCost/12;

//MONTHLY OPERATING CASH FLOW *******************{OK}
  const arrNetMoCashFlow:number[]=[];
  for (var i = 1; i < numTerm; i++) {
    arrNetMoCashFlow.push(MNOIBDS);
  }

//MONTHLY UNLEVERAGED CASH FLOW *********************{UDPATED}
  const arrMULCF:number[]=getMoULCF(numPurchasePrice, numTerm, numExitProceeds, MNOIBDS,numClosingCost)

//YEARLY UNLEVERAGED CASH FLOW***************{UPDATED}
var arrAULCF:number[]=getYrULCF(arrMULCF, numTerm, numPurchasePrice,numClosingCost);

// MONTHLY DEBT SERVICE *********************************** {UPDATED}
  var MoDebtPay:number=getMoDebtService(numPrincipal,numTerm, numInterest, numClosingCost);
  var arrMoBalance:number[]=getBalance(numPrincipal,numTerm, numInterest, numClosingCost);

// YEARLY DEBT SERVICE CASH FLOW: ***********************
  var arrYrDebtPay:number[]=getYrDebtService(numPrincipal,numTerm, numInterest, numClosingCost);

  
//MONTHLY LEVERAGED CASH FLOW: *******************{UPDATED}
//  cashflows inclusive of the debt servicing
const arrMLCF:number[]=getMoLCF(numPrincipal, numTerm, numClosingCost, arrMULCF, MoDebtPay,numPurchasePrice);

// YEARLY LEVERAGED CASH FLOW: ***********************
var arrALCF:number[]=getYrLCF(arrMLCF, numTerm,numPurchasePrice,numClosingCost,numPrincipal);



// FINANCIAL RATIOS

  // YEARLY DSCR Debt Service Coverage Ratio = Net Op Inc / Annual Debt Svc
  const arrYrDSCR:number[]=[];
  for (var i = 0; i < arrALCF.length; i++) {
    arrYrDSCR.push(MNOIBDS/arrYrDebtPay[i]);
  }
 

  // IRR
  //  -- the rate of interest that produces a net present value of zero when accounting for net outlays and income
  const IRRLEV:number = irr(arrALCF)
  console.log("Internal Rate of Return: "+Math.round(IRRLEV*100)/100 +"%")

  const IRRUNLEV:number = irr(arrAULCF)
  console.log("Internal Rate of Return: "+Math.round(IRRUNLEV*100)/100 +"%")
 

  // LEVERAGED MULTIPLE
  //  -- the number of dollars you can expect to get back over the life of the investment for every dollar you put in 
  //  -- = ((sum aMLCF) + Equity) / (Equity)
  //  -- where Equity = purchcost + closecost+entercost+exitcost-Debt
  //  -- where Debt = principal + closecost   (NOTE this assumes close costs are rolled into loan)


var sumaMLCF:number=SumCash(arrMLCF)
var sumaMULCF:number=SumCash(arrMULCF)
var levMULT = (sumaMLCF+EQUITY)/EQUITY;
var unlevMULT = (sumaMULCF+EQUITY)/EQUITY;


// NET PRESENT VALUE
// -- the value of the entire investment in terms of today's money after discounting future cashflows.  
var NPV:number= GetNPV(arrMLCF,numInterest/12);


//DISPLAY ON CONSOLE

  return(
  <div>
<div>
  <br></br>
  <br></br>
  <br></br>
  <br></br>

</div>

    <div>
      
      <h2>ANALYSIS INFORMATION</h2>
      <hr></hr>
    </div>
      <Table.Header>
        <Table.Row className='finceltitle'>MONTHLY CASH FLOWS</Table.Row>

        <Table.Row>
          <Table.HeaderCell className="fincelitemheader">Item</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Month 0</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Month 1</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Month 2</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader"> . . . </Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">(End Term) Month {arrMULCF.length-1}</Table.HeaderCell>
        </Table.Row>

        <Table.Row>
          <Table.Cell className="fincelitem">Gross Rental Revenue</Table.Cell>
          <Table.Cell className="fincelnumber"></Table.Cell>
          <Table.Cell className="fincelnumber">$   {formatToCurrency(numRental)}   </Table.Cell>
          <Table.Cell className="fincelnumber">$   {formatToCurrency(numRental)}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">$   {formatToCurrency(numRental)}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Less Vacancy</Table.Cell>
          <Table.Cell className="fincelnumber"> </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(numRental*numVacancy)}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(numRental*numVacancy)}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(numRental*numVacancy)}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Net Rental Revenue</Table.Cell>
          <Table.Cell className="fincelnumber">  </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(grossIncome)}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(grossIncome)}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(grossIncome)}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Less Op Costs (Tax, Repair, Ins, Util)</Table.Cell>
          <Table.Cell className="fincelnumber">  </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(numOperatingCost+numTaxCost/12)}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(numOperatingCost+numTaxCost/12)}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(numOperatingCost+numTaxCost/12)}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Net Rev before Debt Service </Table.Cell>
          <Table.Cell className="fincelnumber"></Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(MNOIBDS)}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(MNOIBDS)}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(MNOIBDS)}   </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell className="fincelitem">Purch/Sale Proceeds</Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(-numPurchasePrice-numClosingCost)}  </Table.Cell>
          <Table.Cell className="fincelnumber"></Table.Cell>
          <Table.Cell className="fincelnumber"></Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(numExitProceeds)}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Unleveraged Cash</Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMULCF[0])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMULCF[1])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMULCF[2])}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMULCF[arrMULCF.length-1])}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Debt Service</Table.Cell>
          <Table.Cell className="fincelnumber"> </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(MoDebtPay)}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(MoDebtPay)}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(MoDebtPay)}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Loan Balance</Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMoBalance[0])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMoBalance[1])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMoBalance[2])}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMoBalance[arrMoBalance.length-1])}   </Table.Cell>
        </Table.Row>
        
        <Table.Row>
          <Table.Cell className="fincelitem">Leveraged Cash</Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMLCF[0])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMLCF[1])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMLCF[2])}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrMLCF[arrMLCF.length-1])}   </Table.Cell>
        </Table.Row>

<br></br>
<br></br>
        <Table.Row className='finceltitle'>ANNUAL CASH FLOWS</Table.Row>
        <Table.Row>
          <Table.HeaderCell className="fincelitemheader">Item</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Year 0</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Year 1</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Year 2</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader"> . . . </Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">(End Term) Year {arrAULCF.length-1}</Table.HeaderCell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Unleveraged Cash</Table.Cell>
          <Table.Cell className="fincelnumber">$   {formatToCurrency(arrAULCF[0])}   </Table.Cell>
          <Table.Cell className="fincelnumber">$   {formatToCurrency(arrAULCF[1])}   </Table.Cell>
          <Table.Cell className="fincelnumber">$   {formatToCurrency(arrAULCF[2])}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">$   {formatToCurrency(arrAULCF[arrAULCF.length-1])}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Debt Service</Table.Cell>
          <Table.Cell className="fincelnumber"></Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrYrDebtPay[1])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrYrDebtPay[2])}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrYrDebtPay[arrYrDebtPay.length-1])}   </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Leveraged Cash</Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrALCF[0])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrALCF[1])}   </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrALCF[2])}   </Table.Cell>
          <Table.Cell className="fincelnumber">    </Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrALCF[arrALCF.length-1])}   </Table.Cell>
        </Table.Row>

</Table.Header>
<hr></hr>
<Table.Header>
        <Table.Row className='finceltitle'>INVESTMENT STATISTICS</Table.Row>
        <Table.Row>
          <Table.HeaderCell className="fincelitemheader">Item</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Statistic</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Item</Table.HeaderCell>
          <Table.HeaderCell className="fincelcolheader">Statistic</Table.HeaderCell>
        </Table.Row>
        <Table.Row>
          <Table.Cell className="fincelitem">Debt</Table.Cell>
          <Table.Cell className="fincelnumber">$  {formatToCurrency(DEBT)}</Table.Cell>
          <Table.Cell className="fincelnumber">Equity</Table.Cell>
          <Table.Cell className="fincelnumber">$  {formatToCurrency(EQUITY)}</Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell className="fincelitem">IRR-Leveraged </Table.Cell>
          <Table.Cell className="fincelnumber">{Math.round(IRRLEV*10000)/100}%</Table.Cell>
          <Table.Cell className="fincelnumber">IRR-Unleveraged </Table.Cell>
          <Table.Cell className="fincelnumber">{Math.round(IRRUNLEV*10000)/100}%</Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell className="fincelitem">Leveraged MULT</Table.Cell>
          <Table.Cell className="fincelnumber">{Math.round(levMULT*100)/100}x</Table.Cell>
          <Table.Cell className="fincelnumber">Unleveraged MULT</Table.Cell>
          <Table.Cell className="fincelnumber">{Math.round(unlevMULT*100)/100}x</Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell className="fincelitem">NPV</Table.Cell>
          <Table.Cell className="fincelnumber">$  {formatToCurrency(NPV)}</Table.Cell>
{/*           <Table.Cell className="fincelnumber">DSCR (year 1)</Table.Cell>
          <Table.Cell className="fincelnumber">{formatToCurrency(arrYrDSCR[1])}</Table.Cell> */}
        </Table.Row>
      </Table.Header>
   
  </div>
  )
  
}

export default Calculations;

