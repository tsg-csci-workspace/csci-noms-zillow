import  { useEffect, useRef } from 'react';
import { MapContainer, TileLayer, FeatureGroup , Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-draw/dist/leaflet.draw.css';
import { EditControl } from 'react-leaflet-draw';
import 'leaflet-draw';
import * as L from 'leaflet';
import { setLayer, resetLayer } from '../features/mapSlice';
import { leafletElement } from '../models/leafletElement';
import { latLng } from "../models/leafletElement";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from '../app/store';
import { Bundle } from '../models/places.model';
import { usePlacesQuery, useFilterPlacesQuery } from '../features/placeApi';
import houseIcon from '../images/house.png';
import { setPlace } from '../features/placeSlice';

let limit: number = 20;
let latitude: number = 34.11589652;
let longitude: number = -119.12;
let radius: number = 1;

const Leaflet = () => {

    const dispatch = useDispatch();
    const featureGroupRef = useRef<L.FeatureGroup>(null);
    const markerGroupRef = useRef<L.FeatureGroup>(null);
    const leafletLayer = useSelector((state: RootState)=> state.map);
    const filters = useSelector((state: RootState)=> state.filters);
    const placeState = useSelector((state:RootState) =>state.place);
    let queryTemp: string = "";

    const onCreated = e =>{     
        
        if(featureGroupRef.current){
            let drawnItems: L.Layer[] = featureGroupRef.current.getLayers();
        
            if(drawnItems.length > 1){
                drawnItems.forEach((element, index) => {              
                    if(index > 0) return;
                    featureGroupRef.current?.removeLayer(element);
                });
            }
            const currentL = featureGroupRef.current.getLayers()[0];
            const leafElement = createLeafletElement(currentL);
            dispatch(setLayer(leafElement));
        }               
    }

    const onEdited = e =>{
        if(featureGroupRef.current){
            let drawnItems: L.Layer[] = featureGroupRef.current.getLayers();
            const leafElement = createLeafletElement(drawnItems[0]);
            dispatch(setLayer(leafElement));
        }
    }
    const onMounted = e =>{
            //console.log(e);
            var circleTest = L.circle([latitude,longitude],radius*1.609);
            featureGroupRef.current?.addLayer(circleTest);

    }
    const onDeleted = e =>{
        const bundle: Bundle = {} as Bundle;
        dispatch(resetLayer());
        dispatch(setPlace(bundle))
        markerGroupRef.current?.clearLayers();
    }

    const createLeafletElement = (Layer: L.Layer): leafletElement => {
        let templeafLetElement: leafletElement = {} as leafletElement;

        if (Layer instanceof L.Circle){
            let tempLatlng: latLng = {} as latLng
            tempLatlng.lat = Layer.getLatLng().lat;
            tempLatlng.lng = Layer.getLatLng().lng;
            templeafLetElement.latLng = tempLatlng;
            templeafLetElement.radius = Math.trunc(Layer.getRadius() / 1.609);;
            templeafLetElement.layerType = 'circle';
        }else if(Layer instanceof L.Rectangle){
            templeafLetElement.latLng = Layer.getLatLngs();
            templeafLetElement.radius = 0;
            templeafLetElement.layerType = 'rectangle';
        }
        return templeafLetElement;
    };

    const getParameters = () =>{
        switch(leafletLayer.layerType){
            case 'circle':{
                const latlngTemp =  leafletLayer.latLng as latLng;
                latitude = latlngTemp.lat;
                longitude = latlngTemp.lng;
                radius = leafletLayer.radius;
            }
            break;
            default:{
                const latlngTemp =  leafletLayer.latLng as latLng;
                latitude = latlngTemp.lat;
                longitude = latlngTemp.lng;
                radius = leafletLayer.radius;
            }
        }

        if(filters.priceMax === 0 && filters.daysOldest === 0 ){
            queryTemp = `ListPrice.gt=${filters.priceMin},BedroomsTotal.gt=${filters.bedsMin},YearBuilt.gt=${filters.yearOldest},RoomsTotal.gt=${filters.roomsMin}`;
        }
        else if(filters.priceMax != 0 && filters.daysOldest === 0  ){
            queryTemp = `ListPrice.gt=${filters.priceMin},ListPrice.lt=${filters.priceMax},BedroomsTotal.gt=${filters.bedsMin},YearBuilt.gt=${filters.yearOldest},RoomsTotal.gt=${filters.roomsMin}`;
        }
        else if(filters.priceMax === 0 && filters.daysOldest != 0){
            queryTemp = `ListPrice.gt=${filters.priceMin},BedroomsTotal.gt=${filters.bedsMin},YearBuilt.gt=${filters.yearOldest},RoomsTotal.gt=${filters.roomsMin},DaysOnMarket.lt=${filters.daysOldest}`;
        }
       else{
        queryTemp = `ListPrice.gt=${filters.priceMin},ListPrice.lt=${filters.priceMax},BedroomsTotal.gt=${filters.bedsMin},YearBuilt.gt=${filters.yearOldest},RoomsTotal.gt=${filters.roomsMin},DaysOnMarket.lt=${filters.daysOldest}`;
       }
    };

    getParameters();
    //THIS IS OLD API CALL
    //const {data, error, isLoading, isFetching, isSuccess}=usePlacesQuery({limit: limit, latitude: latitude, longitude: longitude, radius: radius});
    const {data, error, isLoading, isFetching, isSuccess} = useFilterPlacesQuery({limit: limit, latitude: latitude, longitude: longitude, radius: radius, query: queryTemp})
    
    const myHouseIcon = L.icon({
      iconUrl: houseIcon,
      iconSize: [49,37],
      popupAnchor: [-10, -30],
    })

    /*
        NOTE: Once we get the back end filtering completed we will change the enpoint call above
              using the filter state and coordinate information. We will also just delete the filter
              call below as the data coming back should already be filtered from the api. 
    */
    /*const createMarkerss = ( bundle: Bundle[] ) =>{    
        return bundle.filter((place) => (
            place.BathroomsFull >= filters.roomsMin &&
            place.BedroomsTotal >= filters.bedsMin &&
            place.YearBuilt >= filters.yearOldest &&
            (filters.priceMin === 0 || place.ListPrice >= filters.priceMin) && 
            (filters.priceMax === 0 ||place.ListPrice <= filters.priceMax) &&
            (filters.daysOldest === 0 || place.DaysOnMarket <= filters.daysOldest))
        ).map((property, index) => 
             (
                 <Marker icon={myHouseIcon} key={`marker-${index}`} position={[property.Latitude, property.Longitude]}>
                    <Popup>
                         Lat: {property.Latitude}
                         lng: {property.Longitude}
                         Listing Price: {property.ListPrice}
                     </Popup>
                 </Marker>
             )
        )
    }*/  
    const createMarkers = ( bundle: Bundle[] ) =>{    
        return bundle.map((property, index) => 
             (
                 <Marker icon={myHouseIcon} key={`marker-${index}`} position={[property.Latitude, property.Longitude]}>
                    <Popup>
                         Lat: {property.Latitude}
                         lng: {property.Longitude}
                         Listing Price: {property.ListPrice}
                     </Popup>
                 </Marker>
             )
        )
    }
    
        
    useEffect(() => {    
        // Update the document title using the browser API    
        document.title = `Property Search`;  });

    return (

        <div className="mapContainer">
            <MapContainer
                center={[latitude, longitude]}
                zoom={10}
                style={{ height: '490px', width:"100%"}}>

                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
                />

                <FeatureGroup
                    ref={featureGroupRef}>
                    <EditControl position="topright" onMounted={onMounted} onCreated={onCreated} onEdited={onEdited} onDeleted={onDeleted} draw={{
                    rectangle: false,
                    polyline: false,
                    circle: true,
                    circlemarker: false,
                    marker: false,
                    polygon: false
                }} />

                </FeatureGroup>
                <FeatureGroup ref={markerGroupRef}>
                    {(data?.bundle && isSuccess ) && createMarkers(data.bundle)}
                </FeatureGroup>
            </MapContainer>
        </div>
    );
};

export default Leaflet;