import React, { useState } from 'react';
import { useDispatch, useSelector} from 'react-redux';
import '../App.css';
import Calculations from './calculations/Calculations';
import {updateFinance,} from '../features/financeSlice'

export default function Analysis() {
  const dispatch = useDispatch();
  const[show, setShow]=useState(0);

  const askUpdateFinance = (e) => {
    dispatch(
        updateFinance(e)
    );
    setShow(show+1)
};

  return (
    <div>
      <nav>
        <div className="button-container">
        {show===0 && <button className="btn btn-primary" type="submit" onClick={()=>askUpdateFinance(1) }>Show Analysis</button>}
        {show>=1 && <button className="buttonMissing" type="submit" onClick={()=>askUpdateFinance(1) }></button>}
        </div>
        
      </nav>
      <div>
      
        {show>=1 && <Calculations />}
      </div>
    </div>
  )
}
