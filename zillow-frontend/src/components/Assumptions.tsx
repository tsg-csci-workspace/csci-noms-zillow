import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector} from 'react-redux';
import '../App.css'
import { RootState } from '../app/store';
import {addFinance, updateFinance,
    addInterest,
    addPrincipal,
    addTerm,
    addClosingCost,
    addRental, 
    addEntryCost, 
    addOccupancy,
    addOperatingCost, 
    addExitProceeds,
    addTaxCost,
    addPurchasePrice} from '../features/financeSlice'
import Calculations from './calculations/Calculations';

export const Assumptions=():JSX.Element =>{
  
    const dispatch = useDispatch();
    const financeState = useSelector((state:RootState) =>state.finance);
    const placeState = useSelector((state:RootState) =>state.place);

    function handleChange(e: { target: HTMLInputElement; }) {
    //setText(e.target.value);
    }

    const changeInterestHandler = (e) => {
        dispatch(
            addInterest(e)
        );
    };
    const changePrincipalHandler = (e) => {
        dispatch(
            addPrincipal(e)
        );
    };
    const changeTermHandler = (e) => {
        dispatch(
            addTerm(e)
        );
    };
    const changeClosingCostHandler = (e) => {
        dispatch(
            addClosingCost(e)
        );
    };
    const changeRentalHandler = (e) => {
        dispatch(
            addRental(e)
        );
    };
    const changeEntryCostHandler = (e) => {
        dispatch(
            addEntryCost(e)
        );
    };

    const changeOccupancyHandler = (e) => {
        dispatch(
            addOccupancy(e)
        );
    };
    const changeOperatingCostHandler = (e) => {
        dispatch(
            addOperatingCost(e)
        );
    };
    const changeExitProceedsHandler = (e) => {
        dispatch(
            addExitProceeds(e)
        );
    };
    const changeTaxCostHandler = (e) => {
        dispatch(
            addTaxCost(e)
        );
    };
    const changePurchasePriceHandler = (e) => {
        dispatch(
            addPurchasePrice(e)
        );
    };
    const clickViewAnalysisHandler = (e) => {
         dispatch(
            console.log(e)
        ); 

    };

        {/* TESTING USE EFFECT */}
        const [count, setCount] = useState('place');
        
        useEffect(() => {    
            // Update the document title using the browser API    
            document.title = `Zillow Listing ${placeState.ListingId}`;  });

    return (
    
    <div>
        <h5>Assumptions - fields are illustrative until we identify what is needed</h5>
        <form className="form-assumptions">

                        {/* ********   definitions for all of these terms are included in the Calculations component   *********** */}

        <div className="assumptions-left">
        <label className="purchase-price-entry">Property Purchase Price:   </label>
            <input type="number" defaultValue={financeState.purchasePrice} className="form-control" id="purchasePrice" onChange={(e)=>changePurchasePriceHandler(e.target.value)} />
            <br></br>
            
            <label className="principal-entry">Loan Principal:   </label>
            <input type="number" defaultValue={financeState.principal} className="form-control" id="principal" onChange={(e)=>changePrincipalHandler(e.target.value)} />
            <br></br>
            
            <label className="interest-entry">Loan APR Interest Rate (0.05=5%APR):  </label>
            <input type="number" defaultValue={financeState.interest} className="form-control" id="interest" onChange={(e)=>changeInterestHandler(e.target.value)} />
            <br></br>

            <label className="term-entry">Loan Period (months):  </label>
            <input type="number" defaultValue={financeState.term} className="form-control" id="term" onChange={(e)=>changeTermHandler(e.target.value)} />
            <br></br>

            <label className="closing-cost-entry">Total Closing Cost:   </label>
            <input type="number" defaultValue={financeState.closingCost} className="form-control" id="closingCost" onChange={(e)=>changeClosingCostHandler(e.target.value)} />
            <br></br>

            <label className="entry-cost-entry">Total Initial Rehab Cost (one-time):  </label>
            <input type="number" defaultValue={financeState.entryCost} className="form-control" id="entryCost" onChange={(e)=>changeEntryCostHandler(e.target.value)} />
            <br></br>

        </div>


        <div className="assumptions-right">

        <label className="rental-entry">Monthly Net Rental Revenue (gross less asset mgmt):  </label>
            <input type="number" defaultValue={financeState.rental} className="form-control" id="rental" onChange={(e)=>changeRentalHandler(e.target.value)} />
            <br></br>

            <label className="operating-cost-entry">Monthly Operating Costs: </label>
            <input type="number" defaultValue={financeState.operatingCost} className="form-control" id="operatingCost" onChange={(e)=>changeOperatingCostHandler(e.target.value)} />
            <br></br>

            <label className="annual-tax-entry">Annual Tax and Insurance: </label>
            <input type="number" defaultValue={financeState.taxCost} className="form-control" id="taxCost" onChange={(e)=>changeTaxCostHandler(e.target.value)} />
            <br></br>

            <label className="occupancy-entry">Expected Occupancy Rate:  </label>
            <input type="number" defaultValue={financeState.occupancy} className="form-control" id="occupancy" onChange={(e)=>changeOccupancyHandler(e.target.value)} />
            <br></br>

            <label className="exit-proceeds-entry">Total Sales Proceed (sales less costs less rehab costs):  </label>
            <input type="number" defaultValue={financeState.exitProceeds} className="form-control" id="exitProceeds" onChange={(e)=>changeExitProceedsHandler(e.target.value)} />
            <br></br>
            <br></br>
                  <br></br>
                  <br></br>
                  <br></br>

            {/* <button className="btn btn-info navbar-btn" type="submit">Update</button> */}
        </div>
    </form>   
</div>
  );
}
export default Assumptions;