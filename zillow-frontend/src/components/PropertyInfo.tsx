import { useSelector, useDispatch } from "react-redux";
import { RootState } from '../app/store';

export default function PropertyInfo() {
    const place=useSelector((state: RootState)=>state.place);

/*
    ListingId: "1",
    //ShortDescription: "999 Hucckls",
    Latitude: 45,
    Longitude: -95,
    RoomsTotal: 4,
    BedroomsTotal: 2,
    BathroomsFull: 1,
    BathroomsTotalDecimal:2,
    YearBuilt: 1985,
    DaysOnMarket:33,
    ListPrice:333333,
    LivingArea:2400,
*/

    return (
      <div>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-6">
                <label>ListingID: {place.ListingId}</label> <br/>
                <label>RoomsTotal: {place.RoomsTotal}</label> <br/>
                <label>BedroomsTotal: {place.BedroomsTotal}</label> <br/>
                <label>BathroomsFull: {place.BathroomsFull}</label> <br/>
                <label>BathroomsTotalDecimal: {place.BathroomsTotalDecimal}</label> <br/>
              </div>
              <div className="col-sm-6">
                <label>LivingArea: {place.LivingArea}</label> <br/>
                <label>YearBuilt: {place.YearBuilt}</label> <br/>
                <label>DaysOnMarket: {place.DaysOnMarket}</label> <br/>
                <label>ListPrice: {place.ListPrice?.toLocaleString('en-us', {  style: 'currency', currency: 'USD' })}</label> <br/>
              </div>
            </div>
          </div>
      </div>
      );

}