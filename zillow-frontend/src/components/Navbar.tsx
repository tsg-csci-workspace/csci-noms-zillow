import React from 'react'
import { NavLink } from 'react-router-dom';

const Navbar = () => {
  return (
    <>
        <div className="navbar-header">
            <NavLink to="/"  className="navbar-brand">
                <h1>Logo</h1>
            </NavLink>


            <div className="col-sm-3 sidenav">
                <NavLink to="/home" className="nav nav-pills nav-stacked">
                    Home
                </NavLink>
                <NavLink to="/analysis" className="menu-analysis">
                    Analysis
                </NavLink>

            </div>

        </div>
    </>
  );
};

export default Navbar;