import React from 'react';
import { RootState } from '../app/store';
import { placeSlice } from '../features/placeSlice';
import { placeApi } from '../features/placeApi';
import { useSelector } from "react-redux";
import { latLng } from "../models/leafletElement";

export default function Summary() {
  const placeCount = placeSlice.reducer.length;
  const leafletElement = useSelector((state: RootState)=> state.map);
  let limit: number = 20;
  let latitude: number = 34.11589652;
  let longitude: number = -119.12;
  let radius: number = 1;
  //code showing how we can get state from other components
  if(leafletElement.layerType == 'circle'){
    const latlngTemp =  leafletElement.latLng as latLng;
    latitude = latlngTemp.lat;
    longitude = latlngTemp.lng;
    radius = leafletElement.radius;
  }

const {bundle} = placeApi.endpoints.places.useQueryState({limit: limit, latitude: latitude, longitude: longitude, radius: radius},{
  selectFromResult: ({ data }) => ({
    bundle: data?.bundle,
  }),
})

//This can probably done a little cleaner I was just trying to get code to work
let averagePrice: number | undefined = undefined;
const total = bundle?.reduce((a,curr) => a + curr.ListPrice, 0);
if(total && bundle?.length){
  averagePrice = total / bundle?.length;
}

  return <div>
  <div>{bundle?.length} Listings, Avg List Price: {averagePrice?.toLocaleString('en-us', {  style: 'currency', currency: 'USD' })}</div>
  <div>Your filter criteria were ... x y z</div>
  </div>
}
