import React, { useState } from 'react';
import _ from 'lodash';
import { useSelector, useDispatch} from 'react-redux';
import { Button, Dropdown, Label } from 'semantic-ui-react';
import '../App.css'
import { RootState } from '../app/store';
import rootReducer from '../app/xxxrootReducer';
import {addBedsMin, addDaysOldest, addFilter, addPriceMax, addPriceMin, addRoomsMin, addYearOldest, clearFilter} from '../features/filterSlice'
import { FilterState } from '../models/filter.model';

export const Filters=() =>{

  const filters = useSelector((state: RootState)=> state.filters);
  const dispatch = useDispatch()

const addFilterHandler = (e) => {
    //e.preventDefault();
    dispatch(
      clearFilter()
    );
  }; 

  const changeMinPriceHandler = (e, result) => {
    dispatch(
      addPriceMin(result.value)
    );
  };
  const changeMaxPriceHandler = (e, result) => {
    dispatch(
      addPriceMax(result.value)
    );
  };
  const changeRoomsMinHandler = (e, result) => {
    dispatch(
      addRoomsMin(result.value)
    );
  };
  const changeBedsMinHandler = (e, result) => {
    dispatch(
      addBedsMin(result.value)
    );
  };
  const changeYearOldestHandler = (e, result) => {
    dispatch(
      addYearOldest(result.value)
    );
  };
  const changeDaysOldestHandler = (e, result) => {
    dispatch(
      addDaysOldest(result.value)
    );
  };

  /*const ListPriceOptions = [
    { key: 100000, text: '$100,000+', value: 1000000 },
    { key: 200000, text: '$200,000+', value: 2000000 },
    { key: 300000, text: '$300,000+', value: 3000000 },
    { key: 400000, text: '$400,000+', value: 4000000 },
    { key: 500000, text: '$500,000+', value: 5000000 },
    { key: 600000, text: '$600,000+', value: 6000000 },
    { key: 700000, text: '$700,000+', value: 7000000 },
    { key: 800000, text: '$800,000+', value: 8000000 },
    { key: 900000, text: '$900,000+', value: 9000000 },
  ]*/

  const MinListPriceOptions = (number, prefix = '$',suffix = '+') =>
  _.times(number, (index) => ({
    key: index*100000,
    text: `${prefix}${index*100000}${suffix}`,
    value: index*100000,
  }))
  const MaxListPriceOptions = (number, prefix = '$',suffix = "") =>
  _.times(number, (index) => ({
    key: index*100000,
    text: `${prefix}${index*100000}${suffix}`,
    value: index*100000,
  }))

  const GetOptions = (number,suffix = '+') =>
  _.times(number, (index) => ({
    key: index,
    text: `${index}${suffix}`,
    value: index,
  }))

  const DayOptions = (number,suffix = ' days ago') =>
  _.times(number, (index) => ({
    key: index,
    text: `${index}${suffix}`,
    value: index,
  }))

  const YearOptions = (number,suffix = '+') =>
  _.times(number, (index) => ({
    key: index+1900,
    text: `${index+1900}${suffix}`,
    value: index+1900,
  }))
  return (
    <form className='add-filter'>
    <div>

        <div className="form-group">
          <Label basic>Min List Price</Label>
            <Dropdown
              search
              searchInput={{ type: 'number' }}
              selection
              options={MinListPriceOptions(10)}
              className="form-control"
              id="priceMin"
              placeholder='Select amount...'
              onChange={changeMinPriceHandler}
              value={filters.priceMin}
            />
        </div>

        <div className="form-group">
          <Label basic>Max List Price</Label>
            <Dropdown
              search
              searchInput={{ type: 'number' }}
              selection
              options={MaxListPriceOptions(11)}
              className="form-control"
              id="priceMax"
              placeholder='Select amount...'
              onChange={changeMaxPriceHandler}
              value={filters.priceMax}
            />
        </div>


        <div className="form-group">
          <Label basic>Min Bathrooms</Label>
            <Dropdown
              search
              searchInput={{ type: 'number' }}
              selection
              options={GetOptions(6)}
              className="form-control"
              id="roomsMin"
              placeholder='Select amount...'
              onChange={changeRoomsMinHandler}
              value={filters.roomsMin}
            />
        </div>

        <div className="form-group">
          <Label basic>Min Bedrooms</Label>
            <Dropdown
              search
              searchInput={{ type: 'number' }}
              selection
              options={GetOptions(6)}
              className="form-control"
              id="bedsMin"
              placeholder='Select amount...'
              onChange={changeBedsMinHandler}
              value={filters.bedsMin}
            />
        </div>


        <div className="form-group">
          <Label basic>Oldest Built Year</Label>
            <Dropdown
              search
              searchInput={{ type: 'number' }}
              selection
              options={YearOptions(123)}
              className="form-control"
              id="yearOldest"
              placeholder='Select Year...'
              onChange={changeYearOldestHandler}
              value={filters.yearOldest}
            />
        </div>

        <div className="form-group">
          <Label basic>Max Number of Days Listed</Label>
            <Dropdown
              search
              searchInput={{ type: 'number' }}
              selection
              options={DayOptions(721)}
              className="form-control"
              id="daysOldest"
              placeholder='Select Days...'
              onChange={changeDaysOldestHandler}
              value={filters.daysOldest}
            />
        </div>
    </div>
    <div>
              { /*<Button type="button" className="btn btn-primary" onClick={(e)=>addFilterHandler([Date.now(),"22","3","4","5","6" ])}>
          Filter Your List of Properties
            </Button>*/}
          <Button type="button" className="btn btn-primary" onClick={(e)=>addFilterHandler([Date.now(),"22","3","4","5","6" ])}>
          Clear Filters
          </Button>
    </div>
    </form>
  );
}


export default Filters;
