import { useSelector, useDispatch } from "react-redux";
import { RootState } from '../app/store';
import { setPlace } from "../features/placeSlice";
import { latLng } from "../models/leafletElement";
import { placeApi } from '../features/placeApi';
import {Link} from 'react-router-dom';
import { Bundle } from "../models/places.model";
import React from 'react'
import { Table, Button } from 'semantic-ui-react'

export default function Places() {
  const leafletElement = useSelector((state: RootState)=> state.map);
  const filters = useSelector((state: RootState)=> state.filters);
  const dispatch = useDispatch();

  let limit: number = 20;
  let latitude: number = 34.11589652;
  let longitude: number = -119.12;
  let radius: number = 1;
  let queryTemp: string = "";
  
  const getParameters = () =>{
    if(leafletElement.layerType === 'circle'){
      const latlngTemp =  leafletElement.latLng as latLng;
      latitude = latlngTemp.lat;
      longitude = latlngTemp.lng;
      radius = leafletElement.radius;
    }
    if(leafletElement.layerType === 'rectangle'){
      //We will add this in later
    }
  
   if(filters.priceMax === 0 && filters.daysOldest === 0 ){
      queryTemp = `ListPrice.gt=${filters.priceMin},BedroomsTotal.gt=${filters.bedsMin},YearBuilt.gt=${filters.yearOldest},RoomsTotal.gt=${filters.roomsMin}`;
    }
    else if(filters.priceMax != 0 && filters.daysOldest === 0  ){
      queryTemp = `ListPrice.gt=${filters.priceMin},ListPrice.lt=${filters.priceMax},BedroomsTotal.gt=${filters.bedsMin},YearBuilt.gt=${filters.yearOldest},RoomsTotal.gt=${filters.roomsMin}`;
    }
    else if(filters.priceMax === 0 && filters.daysOldest != 0){
      queryTemp = `ListPrice.gt=${filters.priceMin},BedroomsTotal.gt=${filters.bedsMin},YearBuilt.gt=${filters.yearOldest},RoomsTotal.gt=${filters.roomsMin},DaysOnMarket.lt=${filters.daysOldest}`;
    }
    else{
      queryTemp = `ListPrice.gt=${filters.priceMin},ListPrice.lt=${filters.priceMax},BedroomsTotal.gt=${filters.bedsMin},YearBuilt.gt=${filters.yearOldest},RoomsTotal.gt=${filters.roomsMin},DaysOnMarket.lt=${filters.daysOldest}`;
    }
  };
 
  //THIS IS OLD API CALL
  //const {data, error, isLoading, isFetching, isSuccess} = placeApi.endpoints.places.useQueryState({limit: limit, latitude: latitude, longitude: longitude, radius: radius});
  getParameters();
  const {data, error, isLoading, isFetching, isSuccess} = placeApi.endpoints.filterPlaces.useQueryState({limit: limit, latitude: latitude, longitude: longitude, radius: radius, query: queryTemp})

  const goAssumptionsHandler = (e) => {
    let bundle: Bundle | undefined;
    if(data?.bundle){
      bundle = data.bundle.find(place=> place.ListingId === e)
    }
    if(bundle){
      dispatch(setPlace(bundle));
    }
  };
  /*
  NOTE: Once we get the back end filtering completed we will change the enpoint call above
        using the filter state and coordinate information. We will also just delete the filter
        call below as the data coming back should already be filtered from the api. 
  */

        /*const createTable = (bundle: Bundle[] ) =>{
          return bundlefilter((place) => (
            place.BathroomsFull >= filters.roomsMin &&
            place.BedroomsTotal >= filters.bedsMin &&
            place.YearBuilt >= filters.yearOldest &&
            (filters.priceMin === 0 || place.ListPrice >= filters.priceMin) && 
            (filters.priceMax === 0 ||place.ListPrice <= filters.priceMax) &&
            (filters.daysOldest === 0 || place.DaysOnMarket <= filters.daysOldest))
          ).map((place, index) => (
            <Table.Row className="data" key={ place.ListingId}>             
               <Table.Cell className="tablecell">{place.ListingId})   </Table.Cell>
               <Table.Cell className="tablecell">{place.LivingArea} sqft. </Table.Cell>
               <Table.Cell className="tablecell"> {place.ListPrice.toLocaleString('en-us', {  style: 'currency', currency: 'USD' })}   </Table.Cell>
               <Table.Cell className="tablecell"> ({place.Latitude}, {place.Longitude})   </Table.Cell>
               <Table.Cell className="tablecell"> {place.RoomsTotal}   </Table.Cell>
               <Table.Cell className="tablecell"> {place.BedroomsTotal}  </Table.Cell>
               <Table.Cell className="tablecell"> {place.BathroomsTotalDecimal}   </Table.Cell>
               <Table.Cell className="tablecell">{place.BathroomsFull}   </Table.Cell>
               <Table.Cell className="tablecell"> {place.YearBuilt}   </Table.Cell>
               <Table.Cell className="tablecell"> {place.DaysOnMarket} days</Table.Cell>
               <Table.Cell>
                  <Link style={{color: "white"}} to="/analysis">
                    <Button secondary type="button" className="btn btn-primary" onClick={(e)=>goAssumptionsHandler(place.ListingId)}>
                      Launch Analysis
                    </Button>
                  </Link>
              </Table.Cell>
            </Table.Row>
            )
          )
        }*/

  const createTable = (bundle: Bundle[] ) =>{
    return bundle.map((place, index) => (
      <Table.Row className="data" key={ place.ListingId}>             
         <Table.Cell className="tablecell">{place.ListingId})   </Table.Cell>
         <Table.Cell className="tablecell">{place.LivingArea} sqft. </Table.Cell>
         <Table.Cell className="tablecell"> {place.ListPrice.toLocaleString('en-us', {  style: 'currency', currency: 'USD' })}   </Table.Cell>
         <Table.Cell className="tablecell"> ({place.Latitude}, {place.Longitude})   </Table.Cell>
         <Table.Cell className="tablecell"> {place.RoomsTotal}   </Table.Cell>
         <Table.Cell className="tablecell"> {place.BedroomsTotal}  </Table.Cell>
         <Table.Cell className="tablecell"> {place.BathroomsTotalDecimal}   </Table.Cell>
         <Table.Cell className="tablecell">{place.BathroomsFull}   </Table.Cell>
         <Table.Cell className="tablecell"> {place.YearBuilt}   </Table.Cell>
         <Table.Cell className="tablecell"> {place.DaysOnMarket} days</Table.Cell>
         <Table.Cell>
            <Link style={{color: "white"}} to="/analysis">
              <Button secondary type="button" className="btn btn-primary" onClick={(e)=>goAssumptionsHandler(place.ListingId)}>
                Launch Analysis
              </Button>
            </Link>
        </Table.Cell>
      </Table.Row>
      )
    )
  }

  return (
    <div>
    {isLoading && <h5>...Loading</h5>}
    {isFetching && <h5>...Fetching</h5>}
    {error && <h5>...Error</h5>}
    {isSuccess && (
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell className="tablecell">Listing Id   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Property Sqft   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Listing Price   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">GPS location   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Rooms   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Beds   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Baths   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Baths(full)   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Year Built   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Days Listed on Market   </Table.HeaderCell>
          <Table.HeaderCell className="tablecell">Launch Analysis   </Table.HeaderCell>
        </Table.Row>
        {(data?.bundle && isSuccess ) && createTable(data.bundle)}
      </Table.Header>
    )}
  </div>
  

  );
};

