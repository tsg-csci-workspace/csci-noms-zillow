import './App.css';
import "leaflet-draw/dist/leaflet.draw.css";
import 'semantic-ui-css/semantic.min.css'
import {Link, Route, Routes} from 'react-router-dom';
import FinderView from './views/FinderView';
import AnalysisView from './views/AnalysisView';

//    *****         SEE THE README.TXT FILE             *********
function App() {

  return (
    <div>
      <nav className="navbar navbar-inverse">
          <div className="container-fluid">
            <div className="navbar-header">
              <h2 className="navbar-brand"><Link style={{color: "white"}} to="/">Investment Property Finder</Link></h2>
            </div>
          </div>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-2 sidenav">
                <ul className="nav nav-pills nav-stacked">
                  <li className="btn btn-primary"><Link style={{color: "black"}} to="/" replace={false} >Finder</Link></li>
                  <li className="btn btn-primary"><Link style={{color: "black"}} to="/analysis" replace={false}>Analysis</Link></li>
                  {/*<li><Link to="/analysis">Analysis Results</Link></li>*/}
                </ul>
              </div>
              <div className="col-sm-10">
                <Routes>
                  <Route path="/" element={<FinderView/>}/>
                  <Route path="analysis" element={<AnalysisView/>}/>
                </Routes>
              </div>
            </div>
          </div>
        </nav> 
    </div>
  );
}


export default App;
