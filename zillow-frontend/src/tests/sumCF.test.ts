import {SumCash} from '../components/calculations/sumCF';


test('get sum cash flow of 0 from inputs 0',()=>{
    expect(SumCash([0,0,0,0,0,0,0,0,0,0,0,0,0])).
    toBe(0)
})

test('get sum cash flow of 100 from inputs with negative',()=>{
    expect(SumCash([200,0,0,0,0,0,0,0,0,0,0,0,-100])).
    toBe(100)
})

test('get lev mult of 2 with sum cash flows of 100 and equity of 100',()=>{
    expect(
        (SumCash([200,0,0,0,0,0,0,0,0,0,0,0,-100])+100) / 100).
        toBe(2)
})