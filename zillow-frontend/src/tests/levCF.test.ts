import {getMoLCF} from '../components/calculations/levCF';

//princ:number, termMo:number, ccost:number, moulcf:number[], modp:number,pp:number
test('get lev cash flow of 0 from principal 0',()=>{
    expect(getMoLCF(0,12,0,[0,0,0,0,0,0,0,0,0,0,0,0,0],0,1000)).toStrictEqual([-1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
})