import {GetNPV} from '../components/calculations/netPresentValue';
import { irr } from 'node-irr';

test('get NPV 120 from 12x10 at 0 interest',()=>{
    expect(GetNPV([10,10,10,10,10,10,10,10,10,10,10,10],0)).toBe(120)
})

test('get NPV 676 from 10x100 at 0.1 interest',()=>{
    expect(Math.round(GetNPV([100,100,100,100,100,100,100,100,100,100],0.1))).toBe(676)
})

test('get NPV -424 from -1000 & 9x100 at 0.1 interest',()=>{
    expect(Math.round(GetNPV([-1000,100,100,100,100,100,100,100,100,100],0.1))).toBe(-424)
})

test('get IRR from values out of workbook',()=>{
    expect(Math.round(irr([-1000,100,100,100,100,100,100,100,100,800])*100)/100).toBe(0.08)
})