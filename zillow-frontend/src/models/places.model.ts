export interface Place{
    success: boolean,
    status: number,
    bundle?: Bundle[],
    total: number
}

export interface Bundle{

    ListingId: string,
    //ShortDescription: string,
    Latitude: number,
    Longitude: number,
    RoomsTotal: number,
    BedroomsTotal: number,
    BathroomsFull: number,
    BathroomsTotalDecimal:number,
    YearBuilt: number,
    DaysOnMarket:number,
    ListPrice:number,
    LivingArea:number

}

