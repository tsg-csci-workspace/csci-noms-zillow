export interface leafletElement{
    radius: number,
    latLng: latLng | latLng[] | latLng[][] | latLng[][][],
    layerType: string
}

export interface latLng {
    lat: number,
    lng: number
}
