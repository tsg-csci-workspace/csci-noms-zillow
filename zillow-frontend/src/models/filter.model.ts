export interface FilterState{
    filters?:filter[],
}

export interface filter{
    id: number,
    priceMin: string,
    priceMax: string,
    roomsMin: string,
    bedsMin: string,
    yearOldest: string,
    daysOldest: string
}